db.users.insertMany([
	{
		firstName: "Shane",
		lastName: "Filan",
		email: "shanefilan@mail.com",
		password: "shane_07",
		isAdmin: false
	},
	{
		firstName: "Nicky",
		lastName: "Byrne",
		email: "nickybyrne@mail.com",
		password: "nicky@7",
		isAdmin: false
	},
	{
		firstName: "Mark",
		lastName: "Feehily",
		email: "markfeehily@mail.com",
		password: "mark",
		isAdmin: false
	},
	{
		firstName: "Brian",
		lastName: "McFadden",
		email: "brianmcfadden@mail.com",
		password: "brian",
		isAdmin: false
	},
	{
		firstName: "Kian",
		lastName: "Egan",
		email: "kianEgan@mail.com",
		password: "kian",
		isAdmin: false
	}

])

db.courses.insertMany([
	{
		name: "HTML 101",
		price: 3000,
		isActive: false
	},
	{
		name: "CSS 101",
		price: 3500,
		isActive: false
	},
	{
		name: "JavaScript 101",
		price: 4000,
		isActive: false
	}
])

db.users.find({isAdmin: false})

db.users.updateOne({}, {$set:{isAdmin: true}})

db.courses.updateOne({name: "CSS 101"}, {$set:{isActive: true}})

db.courses.deleteMany({isActive: false})


